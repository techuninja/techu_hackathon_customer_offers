package com.techu.hackathon.customer_offers.enums;

public enum Uris {

    FLAGS_CONFIGURATION_SERVICE("http://ec2-44-241-170-34.us-west-2.compute.amazonaws.com:8084/configuration/flags"),
    OFFERS_SERVICE("http://ec2-44-241-170-34.us-west-2.compute.amazonaws.com:8085/offers");

    private String uri;

    Uris(String uri) {
        this.uri = uri;
    }

    public String getUri(){
        return uri;
    }
}
