package com.techu.hackathon.customer_offers.service;

import com.techu.hackathon.customer_offers.dto.AccountDto;
import com.techu.hackathon.customer_offers.enums.Uris;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class CustomerOfferServiceImpl implements CustomerOfferService {

    @Autowired
    RestTemplate template;

    @Override
    public Object getOffersByAccounts(String id, List<AccountDto> accounts) {
        Map<String, Integer> flags = getFlagsByAccounts(accounts);
        //System.out.println(flags);
        String url = new StringBuilder(Uris.OFFERS_SERVICE.getUri())
                .append("/flags").toString();
        HttpHeaders headers = getInitialHeaders();
        ResponseEntity<Object> response = template.exchange(url, HttpMethod.POST, new HttpEntity<>(flags, headers), Object.class);
        //System.out.println(response.getBody());
        return response.getBody();
    }

    private Map<String, Integer> getFlagsByAccounts(List<AccountDto> accounts) {
        String url = new StringBuilder(Uris.FLAGS_CONFIGURATION_SERVICE.getUri())
                .append("/accounts").toString();
        HttpHeaders headers = getInitialHeaders();
        ResponseEntity<Object> response = template.exchange(url, HttpMethod.POST, new HttpEntity<>(accounts, headers), Object.class);
        //System.out.println(response.getBody());
        return (Map<String, Integer>) response.getBody();
    }

    private HttpHeaders getInitialHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
