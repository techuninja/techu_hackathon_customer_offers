package com.techu.hackathon.customer_offers.service;

import com.techu.hackathon.customer_offers.dto.AccountDto;

import java.util.List;
import java.util.Map;

public interface CustomerOfferService {

    Object getOffersByAccounts(String id, List<AccountDto> accounts);
}
