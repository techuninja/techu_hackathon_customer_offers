package com.techu.hackathon.customer_offers.controller;

import com.techu.hackathon.customer_offers.dto.AccountDto;
import com.techu.hackathon.customer_offers.service.CustomerOfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/customers")
public class CustomerOfferController {

    @Autowired
    CustomerOfferService service;

    @PostMapping(value = "/{id}/offers", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findCustomerOffers(@PathVariable("id") String id, @RequestBody List<AccountDto> accounts) {
        Object offers = service.getOffersByAccounts(id, accounts);
        return new ResponseEntity<>(offers, HttpStatus.OK);
    }
}
