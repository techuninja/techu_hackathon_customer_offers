FROM openjdk:11-jre-slim-buster
EXPOSE 8082
ARG JAR_FILE=target/customer_offers-1.0.0.jar
ADD ${JAR_FILE} customer_offers.jar
ENTRYPOINT ["java","-jar","/customer_offers.jar"]